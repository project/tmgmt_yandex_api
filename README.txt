TMGMT Translator Yandex

Yandex translator plugin for the Translation Management Tools (TMGMT) project. Allows to submit translation jobs for to Yandex Translator.

About Yandex translation API

The API provides access to the Yandex online machine translation service. It supports more than 70 languages and can translate separate words or complete texts. The API makes it possible to embed Yandex.Translate in a mobile app or web service for end users. Or translate large quantities of text, such as technical documentation.

The project of course also supports implicity all the features which are provided by TMGMT like a feature-rich review process, being able to translate different sources and more.


User case: Translate Node type 'Article'
1. Enable TMGMT (tmgmt_content), TMGMT Translator Yandex (tmgmt_yandex_api) and Content Entity Source (tmgmt_content)

TODO
----

