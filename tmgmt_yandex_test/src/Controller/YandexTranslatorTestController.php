<?php
/**
 * @file
 * Contains \Drupal\tmgmt_microsoft_test\Controller\MicrosoftTranslatorTestController.
 */

namespace Drupal\tmgmt_yandex_test\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Mock services for Yandex translator.
 */
class YandexTranslatorTestController {


  /**
   * Key validator helper.
   */
  protected function validateKey(Request $request) {
    if ($request->get('key') != 'correct key') {
      return $this->trigger_response_error('usageLimits', 'keyInvalid', 'Bad Request');
    }
  }

  /**
   * Helper to trigger mok response error.
   *
   * @param string $domain
   * @param string $reason
   * @param string $message
   * @param string $locationType
   * @param string $location
   */
  public function trigger_response_error($domain, $reason, $message, $locationType = NULL, $location = NULL) {

    $response = [
      'error' => [
        'errors' => [
          'domain' => $domain,
          'reason' => $reason,
          'message' => $message,
        ],
        'code' => 400,
        'message' => $message,
      ],
    ];

    if (!empty($locationType)) {
      $response['error']['errors']['locationType'] = $locationType;
    }
    if (!empty($location)) {
      $response['error']['errors']['location'] = $location;
    }

    return new JsonResponse($response);
  }

  /**
   * Page callback for getting the supported languages.
   */
  public function get_languages(Request $request) {

    if ($response = $this->validateKey($request)) {
      return $response;
    }

    $response = [];
    $response['data'] = [
      'en-fr',
      'en-de',
    ];
    $response['data'] = [
      ['en' => 'English'],
      ['de' => 'German'],
      ['fr' => 'French'],
    ];

    return new JsonResponse($response);
  }


  /**
   * Simulate a translation sent back to plugin.
   */
  public function translate() {

    $text = "TEXT";
    $response = [
      'data' => [
        'translations' => [
          ['translatedText' => $text],
        ],
      ],
    ];

    return new JsonResponse($response);
  }
}
